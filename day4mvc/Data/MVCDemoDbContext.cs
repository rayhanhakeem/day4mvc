﻿using day4mvc.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace day4mvc.Data
{
    public class MVCDemoDbContext : DbContext
    {
        public MVCDemoDbContext(DbContextOptions options) : base(options)
        {
        }


        public DbSet<Intern> Interns { get; set; }
    }
}

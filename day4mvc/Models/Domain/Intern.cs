﻿namespace day4mvc.Models.Domain
{
    public class Intern
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string College { get; set; }
        public long Salary { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Position { get; set; }    
    }
}

﻿namespace day4mvc.Models
{
    public class AddInternViewModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string College { get; set; }
        public long Salary { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Position { get; set; }
    }
}
